'use strict'

const Ansi = use('Ansi')
const User = use('App/Model/User')
const Show = use('App/Model/Show')
const Category = use('App/Model/Category')

class Greet {

  static get signature () {
    return '{name}'
  }

  static get description () {
    return 'Command to greet you with your name'
  }

  * handle (options, flags) {
    Ansi.success(`Hello {options.name}`)
  }
}

module.exports = Greet
