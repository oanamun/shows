'use strict'
const User = use('App/Model/User')
const Ansi = use('Ansi')

class Seed {
    static get inject () {
        return ["App/Model/User"]
    }

    static get signature() {
        return '{table?=all}'
    }

    static get description() {
        return 'Command to seed the database tables'
    }

    * handle(options, flags) {
        if (options.table == "all") {
            Ansi.success(options.table)
        }
        else if (options.table == "users") {
            return this.usersTable()
        }
    }

    static usersTable() {
        yield User.create({username: 'oanam', email: "oanamun@gmail.com", password: "password",first_name: "Oana",last_name: "Muntean",role_id: "3"})

        Ansi.success('Created user')
    }
}

module.exports = Seed
