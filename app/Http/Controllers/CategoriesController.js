'use strict'
const Category = use('App/Model/Category')
const Validator = use('Validator')
const Database = use('Database')

class CategoriesController {
    constructor(){
        this.rules = {
            name : 'required|alpha_numeric'
        }
    }

    * index(request, response) {
        const categories = yield Category.all()
        response.json(categories)
    }

    * show(request, response) {
        const category = yield Category.find(request.param('id'))
        if (category.id === undefined) {
            response.notFound('Category not found')
        }
        else {
            response.json(category.attributes)
        }
    }

    * store(request, response) {
        const data = request.all()
        const validation = yield Validator.validate(this.rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            const category = new Category()
            category.name = request.input('name')
            yield category.create()
            response.json(category.attributes)
        }
    }

    * update(request, response) {
        const data = request.all()
        const validation = yield Validator.validate(this.rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            const category = yield Category.find(request.param('id'))
            if (category.id === undefined) {
                response.notFound('Category not found')
            }
            else {
                category.name = request.input('name')
                yield category.update()
                response.json(category.attributes)
            }
        }
    }

    * destroy(request, response) {
        const category = yield Category.find(request.param('id'))
        if (category.id === undefined) {
            response.notFound('Category not found')
        } else {
            yield category.delete()
            response.ok()
        }
    }

    * shows(request, response) {
        const category = yield Category.where('id',request.param('id')).first().with('shows').fetch()
        if (category.toJSON().id === undefined) {
            response.notFound('Category not found')
        }
        else {
            for (let i = 0; i < category.toJSON().shows.length; i++) {
                const query = yield Database('watching').where('watching.show_id', category.toJSON().shows[i].id).avg('rating as rating').first()
                category.toJSON().shows[i].rating = Math.floor(query.rating)
            }
            response.json(category.toJSON().shows)
        }
    }

    * addshow(request, response) {
        const category = yield Category.find(request.input('category_id'))
        if (category.id === undefined) {
            response.notFound('Category not found')
        }
        else {
            const show_id = request.input('show_id')
            yield category.shows().attach(show_id)
            response.json(category.id)
        }
    }
}

module.exports = CategoriesController
