'use strict'

const Discussion = use('App/Model/Discussion')
const Validator = use('Validator')
const Database = use('Database')
const User = use('App/Model/User')
const Show = use('App/Model/Show')
const Episode = use('App/Model/Episode')

class DiscussionsController {
    constructor() {
        this.rules = {
            title: 'required',
            episode_id: 'required|alpha_numeric'
        }
    }

    * index(request, response) {
        const discussions = yield Discussion.all()
        response.json(discussions)
    }

    * show(request, response) {
        const discussion = yield Discussion.find(request.param('id'))

        if (discussion.id === undefined) {
            response.notFound('Discussion not found')
            return;
        }
        const user = yield User.find(discussion.user_id)
        discussion.username = user.username;
        const episode = yield Episode.find(discussion.episode_id)
        discussion.episode = episode.name;
        const show = yield Database("shows").whereIn('id', function () {
            this.select('seasons.show_id').from('seasons').where('seasons.id', episode.season_id)
        }).first()
        discussion.show = show.name;
        discussion.show_id = show.id;
        response.json(discussion.attributes)
    }

    * store(request, response) {
        const data = request.all()
        const validation = yield Validator.validate(this.rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            const discussion = new Discussion()
            discussion.title = request.input('title')
            discussion.user_id = request.user_id
            discussion.episode_id = request.input('episode_id')
            yield discussion.create()
            response.json(discussion.attributes)
        }
    }


    * update(request, response) {
        const data = request.all()
        const validation = yield Validator.validate(this.rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            const discussion = yield Discussion.find(request.param('id'))
            if (discussion.id === undefined) {
                response.notFound('Discussion not found')
            }
            else {
                discussion.title = request.input('title')
                discussion.user_id = request.input('user_id')
                discussion.episode_id = request.input('episode_id')
                yield discussion.update()
                response.json(discussion.attributes)
            }
        }
    }

    * destroy(request, response) {
        const discussion = yield Discussion.find(request.param('id'))
        if (discussion.id === undefined) {
            response.notFound('Discussion not found')
        } else {
            yield discussion.delete()
            response.ok()
        }
    }

    * comments(request, response) {
        const comments = yield Database('discussion_comment').where('discussion_id', request.param('id'))
        if (comments === undefined) {
            response.notFound('Comments not found')
            return;
        }
        for(let i=0;i<comments.length;i++){
            const user = yield User.find(comments[i].user_id)
            comments[i].username = user.username;
        }
        response.json(comments)
    }
}

module.exports = DiscussionsController
