'use strict'

const Episode = use('App/Model/Episode')
const Validator = use('Validator')
const Database = use('Database')

class EpisodesController {
    constructor() {
        this.rules = {
            name: 'required|alpha_numeric',
            date: 'required|date',
            description: 'required',
            season_id: 'required|alpha_numeric'
        }
    }

    * index(request, response) {
        const episodes = yield Episode.all()
        response.json(episodes)
    }

    * show(request, response) {
        const episode = yield Episode.find(request.param('id'))
        if (episode.id === undefined) {
            response.notFound('Episode not found')
            return
        }
        const rating = yield Database('seen').where('seen.episode_id', request.param('id')).avg('rating as value').first()
        episode.rating = Math.floor(rating.value)

        const season = yield Database('seasons').where('seasons.id', episode.season_id).first()
        episode.season = season.number

        const show = yield Database('shows').where('shows.id', season.show_id).first()
        episode.show_id = show.id
        episode.show = show.name

        response.json(episode.attributes)

    }

    * store(request, response) {
        const data = request.all()
        const validation = yield Validator.validate(this.rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            const episode = new Episode()
            episode.date = request.input('date')
            episode.name = request.input('name')
            episode.description = request.input('description')
            episode.season_id = request.input('season_id')
            yield episode.create()
            response.json(episode.attributes)
        }
    }

    * update(request, response) {
        const data = request.all()
        const validation = yield Validator.validate(this.rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            const episode = yield Episode.find(request.param('id'))
            if (episode.id === undefined) {
                response.notFound('Episode not found')
            }
            else {
                episode.date = request.input('date')
                episode.name = request.input('name')
                episode.description = request.input('description')
                episode.season_id = request.input('season_id')
                yield episode.update()
                response.json(episode.attributes)
            }
        }
    }

    * destroy(request, response) {
        const episode = yield Episode.find(request.param('id'))
        if (episode.id === undefined) {
            response.notFound('Episode not found')
        }
        else {
            yield episode.delete()
            response.ok()
        }
    }

    * watchers(request, response) {
        const episode = yield Episode.where('id', request.param('id')).first().with('users').fetch()
        if (episode.toJSON().id === undefined) {
            response.notFound('Episode not found')
        }
        else {
            response.json(episode.toJSON().users)
        }
    }

    * discussions(request, response) {
        const discussions = yield Database('discussions').where('episode_id', request.param('id')).orderBy("created_at","desc")
        if (discussions === undefined) {
            response.notFound('Discussions not found')
            return
        }

        for (let i = 0; i < discussions.length; i++) {
            const user = yield Database('users').where('id', discussions[i].user_id).first()
            discussions[i].username = user.username;

            const comment = yield Database('discussion_comment').where('discussion_comment.discussion_id',discussions[i].id).orderBy('created_at', 'asc').first()
            if(comment.message.length > 200){
                comment.message = comment.message.substring(0,200)+"..."
            }else{
                comment.message = comment.message.substring(0,200)
            }

            discussions[i].comment = comment.message;
        }

        response.json(discussions)
    }

    * rate(request, response) {
        const validation = yield Validator.validate({
            rating: 'required|range:0,6'
        }, request.all())

        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            yield Database('seen').where({user_id: request.user_id, episode_id: request.param('id')})
                .update({rating: request.input('rating')})
            response.ok()
        }
    }

    *rating(request, response) {
        const rating = yield Database('seen').where('seen.episode_id', request.param('id')).avg('rating as rating').first()
        response.json(rating)
    }
}

module.exports = EpisodesController
