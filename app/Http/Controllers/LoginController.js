'use strict'

const User = use('App/Model/User')
const Database = use('Database')
const JWT = use('jwt-simple')
const Env = use('Env')
const Hash = use('Hash')

class LoginController {

    * login(request, response) {
        const user= yield Database.from('users').where('username', request.input('username')).first()
        const isSame = yield Hash.verify(request.input('password'), user.password)

        if (isSame) {
            const payload = {id: user.id, created_at: Math.floor(Date.now() / 1000)};
            const token = JWT.encode(payload, Env.get('JWT_SECRET'));
            user.token = token
            response.json(user)
        }
        else {
            response.unauthorized('Login failed!')
        }
        
    }
}

module.exports = LoginController
