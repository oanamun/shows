'use strict'

const ReviewComment = use('App/Model/ReviewComment')
const Validator = use('Validator')

class ReviewCommentsController {
    constructor() {
        this.rules = {
            message: 'required',
            review_id: 'required|alpha_numeric'
        }
    }

    * index(request, response) {
        const comments = yield ReviewComment.all()
        response.json(comments)
    }

    * show(request, response) {
        const comment = yield ReviewComment.find(request.param('id'))
        if (comment.id === undefined) {
            response.notFound('Comment not found')
        }
        else {
            response.json(comment.attributes)
        }
    }

    * store(request, response) {
        const data = request.all()
        const validation = yield Validator.validate(this.rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            const comment = new ReviewComment()
            comment.message = request.input('message')
            comment.user_id = request.user_id
            comment.review_id = request.input('review_id')
            yield comment.create()
            response.json(comment.attributes)
        }
    }


    * update(request, response) {
        const data = request.all()
        const validation = yield Validator.validate(this.rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            const comment = yield ReviewComment.find(request.param('id'))
            if (comment.id === undefined) {
                response.notFound('Comment not found')
            }
            else {
                comment.message = request.input('message')
                comment.user_id = request.input('user_id')
                comment.review_id = request.input('review_id')
                yield comment.update()
                response.json(comment.attributes)
            }
        }
    }

    * destroy(request, response) {
        const comment = yield ReviewComment.find(request.param('id'))
        if (comment.id === undefined) {
            response.notFound('Comment not found')
        } else {
            yield comment.delete()
            response.ok()
        }
    }

}

module.exports = ReviewCommentsController
