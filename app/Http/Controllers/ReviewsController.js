'use strict'

const Review = use('App/Model/Review')
const Validator = use('Validator')
const Database = use('Database')
const User = use('App/Model/User')
const Show = use('App/Model/Show')

class ReviewsController {
    constructor() {
        this.rules = {
            message: 'required',
            title: 'required',
            show_id: 'required|alpha_numeric'
        }
    }

    * index(request, response) {
        const reviews = yield Review.all()
        response.json(reviews)
    }

    * show(request, response) {
        const review = yield Review.find(request.param('id'))
        if (review.id === undefined) {
            response.notFound('Review not found')
        }
        else {
            const user = yield User.find(review.user_id)
            review.username = user.username;
            const show = yield Show.find(review.show_id)
            review.show = show.name;
            response.json(review.attributes)
        }
    }

    *recent(request, response) {
        const reviews = yield Database('reviews').orderBy('created_at', 'desc').limit(4).offset(0)
        for (let i = 0; i < reviews.length; i++) {
            const user = yield User.find(reviews[i].user_id)
            reviews[i].username = user.username;
            const show = yield Show.find(reviews[i].show_id)
            reviews[i].show = show.name;
            if(reviews[i].message.length > 250){
                reviews[i].message = reviews[i].message.substring(0,250)+"..."
            }else{
                reviews[i].message = reviews[i].message.substring(0,250)
            }
        }
        response.json(reviews)
    }

    * store(request, response) {
        const data = request.all()
        const validation = yield Validator.validate(this.rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            const review = new Review()
            review.user_id = request.user_id
            review.message = request.input('message')
            review.title = request.input('title')
            review.show_id = request.input('show_id')
            yield review.create()
            response.json(review.attributes)
        }
    }


    * update(request, response) {
        const data = request.all()
        const validation = yield Validator.validate(this.rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            const review = yield Review.find(request.param('id'))
            if (review.id === undefined) {
                response.notFound('Review not found')
            }
            else {
                review.review = request.input('review')
                review.user_id = request.input('user_id')
                review.show_id = request.input('show_id')
                yield review.update()
                response.json(review.attributes)
            }
        }
    }

    * destroy(request, response) {
        const review = yield Review.find(request.param('id'))
        if (review.id === undefined) {
            response.notFound('Review not found')
        } else {
            yield review.delete()
            response.ok()
        }
    }

    * comments(request, response) {
        const comments = yield Database('review_comment').where('review_id', request.param('id'))
        if (comments === undefined) {
            response.notFound('Comments not found')
            return
        }
        for (let i = 0; i < comments.length; i++) {
            const user = yield Database('users').where('id',comments[i].user_id).first()
            comments[i].username = user.username;
        }
        response.json(comments)

    }

    * ratings(request, response) {
        const review = yield Review.where('id', request.param('id')).first().with('ratings').fetch()
        if (review.toJSON().id === undefined) {
            response.notFound('Review not found')
        }
        else {
            response.json(review.toJSON().ratings)
        }
    }

    * rate(request, response) {
        const validation = yield Validator.validate({
            rating: 'required|range:0,6'
        }, request.all())

        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            yield Database('user_review').insert({
                user_id: request.user_id,
                review_id: request.param('id'),
                rating: request.input('rating')
            })
            response.ok()
        }
    }
}

module.exports = ReviewsController
