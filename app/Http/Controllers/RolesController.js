'use strict'
const Role = use('App/Model/Role')
const Validator = use('Validator')

class RolesController {
    constructor(){
        this.rules = {
            role : 'required|alpha_numeric'
        }
    }

    * index(request, response) {
        const roles = yield Role.all()
        response.json(roles)
    }

    * show(request, response) {
        const role = yield Role.find(request.param('id'))
        if (role.id === undefined) {
            response.notFound('Role not found')
        }
        else {
            response.json(role.attributes)
        }
    }

    * store(request, response) {
        const data = request.all()
        const validation = yield Validator.validate(this.rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else{
            const role = new Role()
            yield role.create({role: request.input('role')})
            response.json(role.attributes)
        }
    }

    * update(request, response) {
        const role = yield Role.find(request.param('id'))
        if (role.id === undefined) {
            response.notFound('Role not found')
        }
        else {
            role.role = request.input('role')
            yield role.update()
            response.json(role.attributes)
        }
    }

    * destroy(request, response) {
        const role = yield Role.find(request.param('id'))
        if (role.id === undefined) {
            response.notFound('Role not found')
        } else {
            yield role.delete()
            response.ok()
        }
    }

    * users(request, response) {
        const role = yield Role.where('id', request.param('id')).first().with('users').fetch()
        if (role.toJSON().id === undefined) {
            response.notFound('Role not found')
        }
        else {
            response.json(role.toJSON().users)
        }
    }
}

module.exports = RolesController
