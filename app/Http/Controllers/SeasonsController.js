'use strict'

const Season = use('App/Model/Season')
const Validator = use('Validator')

class SeasonsController {
    constructor() {
        this.rules = {
            number: 'required|alpha_numeric',
            start_date: 'required|date',
            end_date: 'required|date',
            show_id: 'required|alpha_numeric'
        }
    }

    * index(request, response) {
        const seasons = yield Season.all()
        response.json(seasons)
    }

    * show(request, response) {
        const season = yield Season.find(request.param('id'))
        if (season.id === undefined) {
            response.notFound('Season not found')
        }
        else {
            response.json(season.attributes)
        }
    }

    * store(request, response) {
        const data = request.all()
        const validation = yield Validator.validate(this.rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            const season = new Season()
            season.number = request.input('number')
            season.start_date = request.input('start_date')
            season.end_date = request.input('end_date')
            season.show_id = request.input('show_id')
            yield season.create()
            response.json(season.attributes)
        }
    }

    * update(request, response) {
        const data = request.all()
        const validation = yield Validator.validate(this.rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            const season = yield Season.find(request.param('id'))
            if (season.id === undefined) {
                response.notFound('Season not found')
            }
            else {
                season.number = request.input('number')
                season.start_date = request.input('start_date')
                season.end_date = request.input('end_date')
                season.show_id = request.input('show_id')
                yield season.update()
                response.json(season.attributes)
            }
        }
    }

    * destroy(request, response) {
        const season = yield Season.find(request.param('id'))
        if (season.id === undefined) {
            response.notFound('Season not found')
        }
        else {
            yield season.delete()
            response.ok()
        }
    }

    * episodes(request, response) {
        const season = yield Season.where('id', request.param('id')).first().with('episodes').fetch()
        if (season.toJSON().id === undefined) {
            response.notFound('Season not found')
        }
        else {
            response.json(season.toJSON().episodes)
        }
    }

}

module.exports = SeasonsController
