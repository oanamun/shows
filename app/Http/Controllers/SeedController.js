'use strict'
const User = use('App/Model/User')
const Show = use('App/Model/Show')
const Season = use('App/Model/Season')
const Episode = use('App/Model/Episode')
const Review = use('App/Model/Review')
const Category = use('App/Model/Category')
const Discussion = use('App/Model/Discussion')
const Role = use('App/Model/Role')

class SeedController {

    * users(request, response) {
        yield User.create({username: 'admin', email: "admin@shows.com", password: "password", role_id: "1"})

        yield User.create({username: 'mod1', email: "mod1@shows.com", password: "password", role_id: "2"})
        yield User.create({username: 'mod2', email: "mod2@shows.com", password: "password", role_id: "2"})
        yield User.create({username: 'mod3', email: "mod3@shows.com", password: "password", role_id: "2"})
        yield User.create({username: 'mod4', email: "mod4@shows.com", password: "password", role_id: "2"})

        yield User.create({
            username: 'nanax',
            email: "oanamuntean8@gmail.com",
            password: "password",
            first_name: "Oana",
            last_name: "Muntean",
            role_id: "3"
        })
        yield User.create({
            username: 'utwo',
            email: "mihai.legat@gmail.com",
            password: "password",
            first_name: "Mihai",
            last_name: "Popa",
            role_id: "3"
        })
        yield User.create({
            username: 'elizel',
            email: "elizanitoi@gmail.com",
            password: "password",
            first_name: "Eliza",
            last_name: "Nitoi",
            role_id: "3"
        })
        yield User.create({
            username: 'oona',
            email: "oona1994@gmail.com",
            password: "password",
            first_name: "Oana",
            last_name: "Petrean",
            role_id: "3"
        })
        yield User.create({
            username: 'darkyshadow',
            email: "sabinabocaneci@gmail.com",
            password: "password",
            first_name: "Sabina",
            last_name: "Bocaneci",
            role_id: "3"
        })
        yield User.create({
            username: 'alexr',
            email: "alexruja@gmail.com",
            password: "password",
            first_name: "Alex",
            last_name: "Ruja",
            role_id: "3"
        })
        yield User.create({
            username: 'mydearestcrime',
            email: "madalinadumitru@gmail.com",
            password: "password",
            first_name: "Madalina",
            last_name: "Dumitru",
            role_id: "3"
        })
        yield User.create({
            username: 'anaivvv',
            email: "anaivautiu@gmail.com",
            password: "password",
            first_name: "Ana",
            last_name: "Ivanutiu",
            role_id: "3"
        })
        yield User.create({
            username: 'razv',
            email: "razvanstetcu@gmail.com",
            password: "password",
            first_name: "Razvan",
            last_name: "Stetcu",
            role_id: "3"
        })
        yield User.create({
            username: 'pope',
            email: "mihaipopescu@gmail.com",
            password: "password",
            first_name: "Mihai",
            last_name: "Popescu",
            role_id: "3"
        })
        yield User.create({
            username: 'bibi',
            email: "biancapopescu@gmail.com",
            password: "password",
            first_name: "Bianca",
            last_name: "Popescu",
            role_id: "3"
        })
        yield User.create({
            username: 'lari',
            email: "larisazah@gmail.com",
            password: "password",
            first_name: "Larisa",
            last_name: "Zah",
            role_id: "3"
        })
        //yield User.create({username: '', email: "@gmail.com", password: "password",first_name: "",last_name: "",role_id: "3"})

        response.ok("Users created")
    }

    * shows(request, response) {
        yield Show.create({
            imdb_id: 'tt0944947',
            name: 'Game of Thrones',
            description: 'While a civil war brews between several noble families in Westeros, the children of the former rulers of the land attempt to rise up to power. Meanwhile a forgotten race, bent on destruction, return after thousands of years in the North. ',
            year: '2011',
            creator: 'David Benioff, D.B. Weiss',
            channel: 'HBO'
        })
        yield Show.create({
            imdb_id: 'tt0903747',
            name: 'Breaking Bad',
            description: "A chemistry teacher diagnosed with inoperable lung cancer turns to manufacturing and selling methamphetamine in order to secure his family's financial future.",
            year: '2008',
            creator: 'Vince Gilligan',
            channel: 'AMC'
        })
        yield Show.create({
            imdb_id: 'tt1475582',
            name: 'Sherlock',
            description: 'A modern update finds the famous sleuth and his doctor partner solving crime in 21st century London.',
            year: '2010',
            creator: ' Mark Gatiss, Steven Moffat ',
            channel: 'BBC'
        })
        yield Show.create({
            imdb_id: 'tt2802850',
            name: 'Fargo',
            description: 'Various chronicles of deception, intrigue and murder in and around frozen Minnesota. Yet all of these tales mysteriously lead back one way or another to Fargo, North Dakota ',
            year: '2013',
            creator: '',
            channel: 'FX'
        })
        yield Show.create({
            imdb_id: 'tt2356777',
            name: 'True Detective',
            description: "In 2012, Louisiana State Police Detectives Rust Cohle and Martin Hart are brought in to revisit a homicide case they worked in 1995. As the inquiry unfolds in present day through separate interrogations, the two former detectives narrate the story of their investigation, reopening unhealed wounds, and drawing into question their supposed solving of a bizarre ritualistic murder in 1995. The timelines braid and converge in 2012 as each man is pulled back into a world they believed they'd left behind. In learning about each other and their killer, it becomes clear that darkness lives on both sides of the law.",
            year: '2014',
            creator: 'Nic Pizzolatto',
            channel: 'HBO'
        })
        yield Show.create({
            imdb_id: 'tt1856010',
            name: 'House of Cards',
            description: "Majority House Whip Francis Underwood takes you on a long journey as he exacts his vengeance on those he feels wronged him - that is, his own cabinet members including the President of the United States himself. Dashing, cunning, methodical and vicious, Frank Underwood along with his equally manipulative yet ambiguous wife, Claire take Washington by storm through climbing the hierarchical ladder to power in this Americanized recreation of the BBC series of the same name. ",
            year: '2013',
            creator: '',
            channel: 'Netflix'
        })
        yield Show.create({
            imdb_id: 'tt0108778',
            name: 'Friends',
            description: "Rachel Green, Ross Geller, Monica Geller, Joey Tribbiani, Chandler Bing and Phoebe Buffay are all friends, living off of one another in the heart of New York. Over the course of ten years, these average group of buddy's go through massive mayhem, family trouble, past and future romances, fights, laughs, tears and surprises as they learn what it really means to be a friend.",
            year: '1994',
            creator: 'David Crane, Marta Kauffman',
            channel: 'NBC'
        })
        yield Show.create({
            imdb_id: 'tt2707408',
            name: 'Narcos',
            description: "Narcos tells the true-life story of the growth and spread of cocaine drug cartels across the globe and attendant efforts of law enforcement to meet them head on in brutal, bloody conflict. It centers around the notorious Colombian cocaine kingpin Pablo Escobar (Wagner Moura) and Steve Murphy (Holbrook), a DEA agent sent to Colombia on a U.S. mission to capture and ultimately kill him.",
            year: '2015',
            creator: 'Carlo Bernard, Chris Brancato, Doug Miro',
            channel: 'Netflix'
        })
        yield Show.create({
            imdb_id: 'tt3322312',
            name: 'Daredevil',
            description: "As a child Matt Murdock was blinded by a chemical spill in a freak accident. Instead of limiting him it gave him superhuman senses that enabled him to see the world in a unique and powerful way. Now he uses these powers to deliver justice, not only as a lawyer in his own law firm, but also as vigilante at night, stalking the streets of Hell's Kitchen as Daredevil, the man without fear.",
            year: '2015',
            creator: 'Drew Goddard',
            channel: 'Netflix'
        })
        yield Show.create({
            imdb_id: 'tt4158110',
            name: 'Mr. Robot',
            description: "Elliot Alderson is a young cyber-security engineer living in New York, who assumes the role of a vigilante hacker by night. Elliot meets a mysterious anarchist known as Mr. Robot who recruits Elliot to join his team of hackers, fsociety. Elliot, who has a social anxiety disorder and connects to people by hacking them, is intrigued but uncertain if he wants to be part of the group. The show follows Mr. Robot's attempts to engage Elliot in his mission to destroy the corporation Elliot is paid to protect. Compelled by his personal beliefs, Elliot struggles to resist the chance to take down the multinational CEOs that are running (and ruining) the world.",
            year: '2015',
            creator: '',
            channel: 'USA Network'
        })
        yield Show.create({
            imdb_id: 'tt0472954',
            name: "It's Always Sunny in Philadelphia",
            description: "Four young friends with big egos and slightly arrogant attitudes are the proprietors of an Irish bar in Philadelphia. ",
            year: '2005',
            creator: 'Rob McElhenney, Glenn Howerton',
            channel: 'FX'
        })
        yield Show.create({
            imdb_id: 'tt1632701',
            name: 'Suits',
            description: "On the run from a drug deal gone bad, Mike Ross, a brilliant college-dropout, finds himself a job working with Harvey Specter, one of New York City's best lawyers.",
            year: '2011',
            creator: 'Aaron Korsh',
            channel: 'USA Network'
        })
        yield Show.create({
            imdb_id: 'tt1606375',
            name: 'Downton Abbey',
            description: "A chronicle of the lives of the British aristocratic Crawley family and their servants in the early 20th Century.",
            year: '2010',
            creator: 'Julian Fellowes',
            channel: 'BBC'
        })
        yield Show.create({
            imdb_id: 'tt2306299',
            name: 'Vikings',
            description: "The world of the Vikings is brought to life through the journey of Ragnar Lothbrok, the first Viking to emerge from Norse legend and onto the pages of history - a man on the edge of myth. ",
            year: '2013',
            creator: 'Michael Hirst',
            channel: 'History Channel'
        })
        yield Show.create({
            imdb_id: 'tt1442437',
            name: 'Modern Family',
            description: "Three different, but related families face trials and tribulations in their own uniquely comedic ways. ",
            year: '2009',
            creator: 'Steven Levitan, Christopher Lloyd',
            channel: 'HBO'
        })
        yield Show.create({
            imdb_id: 'tt0411008',
            name: 'Lost',
            description: "The survivors of a plane crash are forced to work together in order to survive on a seemingly deserted tropical island. ",
            year: '2005',
            creator: 'J.J. Abrams, Jeffrey Lieber, Damon Lindelof',
            channel: 'ABC'
        })
        yield Show.create({
            imdb_id: 'tt1796960',
            name: 'Homeland',
            description: "A bipolar CIA operative becomes convinced a prisoner of war has been turned by al-Qaeda and is planning to carry out a terrorist attack on American soil. ",
            year: '2011',
            creator: 'Alex Gansa, Howard Gordon',
            channel: 'Showtime'
        })
        yield Show.create({
            imdb_id: 'tt1844624',
            name: 'American Horror Story',
            description: "An anthology series that centers on different characters and locations, including a house with a murderous past, an insane asylum, a witch coven, a freak show, and an enigmatic hotel.",
            year: '2011',
            creator: ' Brad Falchuk, Ryan Murphy ',
            channel: 'FX'
        })
        yield Show.create({
            imdb_id: 'tt2357547',
            name: 'Jessica Jones',
            description: "A former superhero decides to reboot her life by becoming a private investigator. ",
            year: '2015',
            creator: 'Melissa Rosenberg',
            channel: 'Netflix'
        })
        yield Show.create({
            imdb_id: 'tt0413573',
            name: "Grey's Anatomy",
            description: "A Medical-Based drama centered around Meredith Grey, an aspiring surgeon and daughter of one of the best surgeons, Dr. Ellis Grey. Throughout the series, Meredith goes through professional and personal challenges along with fellow surgeons at Seattle Grace Hospital. ",
            year: '2005',
            creator: 'Shonda Rhimes',
            channel: 'ABC'
        })
        //yield Show.create({imdb_id: '', name: "", description: "", year: '', creator: '', channel: ''})

        response.ok("Shows created")
    }

    *seasons(request, response) {
        //yield Season.create({number: '1', start_date: '2011-04-17', end_date: '2011-06-19', show_id: '1'})
        //yield Season.create({number: '2', start_date: '2012-04-01', end_date: '2012-06-03', show_id: '1'})
        //yield Season.create({number: '3', start_date: '2013-03-31', end_date: '2013-06-09', show_id: '1'})
        //yield Season.create({number: '4', start_date: '2014-04-06', end_date: '2014-06-15', show_id: '1'})
        //yield Season.create({number: '5', start_date: '2015-04-12', end_date: '2014-06-14', show_id: '1'})
        //yield Season.create({number: '6', start_date: '2016-04-24', end_date: '2014-06-26', show_id: '1'})

        yield Season.create({number: '', start_date: '', end_date: '', show_id: ''})

        response.ok("Seasons created")
    }

    *episodes(request, response) {
        //yield Episode.create({name: 'Winter Is Coming', date: '2011-04-17', description: "Jon Arryn, the Hand of the King, is dead. King Robert Baratheon plans to ask his oldest friend, Eddard Stark, to take Jon's place. Across the sea, Viserys Targaryen plans to wed his sister to a nomadic warlord in exchange for an army. ", season_id: '1'})
        //yield Episode.create({name: "The Kingsroad", date: '2011-04-24', description: "While Bran recovers from his fall, Ned takes only his daughters to Kings Landing. Jon Snow goes with his uncle Benjen to The Wall. Tyrion joins them. ", season_id: '1'})
        //yield Episode.create({name: "Lord Snow", date: '2011-05-01', description: "Lord Stark and his daughters arrive at King's Landing to discover the intrigues of the king's realm.", season_id: '1'})
        //yield Episode.create({name: "Cripples, Bastards, and Broken Things", date: '2011-05-08', description: "Eddard investigates Jon Arryn's murder. Jon befriends Samwell Tarly, a coward who has come to join the Night's Watch. ", season_id: '1'})
        //yield Episode.create({name: "The Wolf and the Lion", date: '2011-05-15', description: "Catelyn has captured Tyrion and plans to bring him to her sister, Lysa Arryn, at The Vale, to be tried for his, supposed, crimes against Bran. Robert plans to have Daenerys killed, but Eddard refuses to be a part of it and quits. ", season_id: '1'})
        //yield Episode.create({name: "A Golden Crown", date: '2011-05-22', description: "While recovering from his battle with Jamie, Eddard is forced to run the kingdom while Robert goes hunting. Tyrion demands a trial by combat for his freedom. Viserys is losing his patience with Drogo. ", season_id: '1'})
        //yield Episode.create({name: "You Win or You Die", date: '2011-05-29', description: "Robert has been injured while hunting and is dying. Jon and the others finally take their vows to the Night's Watch. A man, sent by Robert, is captured for trying to poison Daenerys. Furious, Drogo vows to attack the Seven Kingdoms. ", season_id: '1'})
        //yield Episode.create({name: "The Pointy End", date: '2011-06-05', description: "Eddard and his men are betrayed and captured by the Lannisters. When word reaches Robb, he plans to go to war to rescue them. The White Walkers attack The Wall. Tyrion returns to his father with some new friends. ", season_id: '1'})
        //yield Episode.create({name: "Baelor", date: '2011-06-12', description: "Robb goes to war against the Lannisters. Jon finds himself struggling on deciding if his place is with Robb or the Night's Watch. Drogo has fallen ill from a fresh battle wound. Daenerys is desperate to save him. ", season_id: '1'})
        //yield Episode.create({name: "Fire and Blood", date: '2011-06-19', description: "With Ned dead, Robb vows to get revenge on the Lannisters. Jon must officially decide if his place is with Robb or the Night's Watch. Daenerys says her final goodbye to Drogo. ", season_id: '1'})

        yield Episode.create({name: "", date: '', description: "", season_id: ''})

        response.ok("Episodes created")
    }

    * reviews(request, response) {
        yield Review.create({review: '', user_id: '', show_id: ''})
        response.ok()
    }

    * categories(request, response) {
        yield Category.create({name: 'Action'})
        yield Category.create({name: 'Adventure'})
        yield Category.create({name: 'Animation'})
        yield Category.create({name: 'Biography'})
        yield Category.create({name: 'Comedy'})
        yield Category.create({name: 'Crime'})
        yield Category.create({name: 'Documentary'})
        yield Category.create({name: 'Drama'})
        yield Category.create({name: 'Fantasy'})
        yield Category.create({name: 'Horror'})
        yield Category.create({name: 'History'})
        yield Category.create({name: 'Musical'})
        yield Category.create({name: 'Mystery'})
        yield Category.create({name: 'Romance'})
        yield Category.create({name: 'Sci-Fi'})
        yield Category.create({name: 'Sitcom'})
        yield Category.create({name: 'Sport'})
        yield Category.create({name: 'Thriller'})
        yield Category.create({name: 'War'})
        response.ok()
    }

    * discussions(request, response) {
        yield Discussion.create({title: '', user_id: '', episode_id: ''})
        response.ok()
    }

    * roles(request, response) {
        yield Role.create({role: 'admin'})
        yield Role.create({role: 'mod'})
        yield Role.create({role: 'user'})
        response.ok()
    }
}

module.exports = SeedController
