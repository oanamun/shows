'use strict'

const Show = use('App/Model/Show')
const Validator = use('Validator')
const Database = use('Database')
const User = use('App/Model/User')
const Notification = use('App/Model/Notification')

class ShowsController {
    constructor() {
        this.rules = {
            imdb_id: 'alpha_numeric',
            name: 'required',
            description: 'required',
            year: 'year',
            creator: 'alpha_numeric',
            channel: 'alpha_numeric'
        }
    }

    * index(request, response) {
        const name = request.input('name')
        let shows = (yield Show.all()).toJSON()
        if (name) {
            let filtered = []
            for (let i = 0; i < shows.length; i++) {
                if (shows[i].name.toLowerCase().indexOf(name.toLowerCase()) > -1) {
                    shows[i].url = '/show/' + shows[i].id
                    filtered.push(shows[i])
                }
            }
            shows = {filtered}
        }
        for (let i = 0; i < shows.length; i++) {
            const query = yield Database('watching').where('watching.show_id', shows[i].id).avg('rating as rating').first()
            shows[i].rating = Math.floor(query.rating)
        }
        response.json(shows)
    }

    *recent(request, response) {
        const shows = yield Database('shows').orderBy('created_at', 'desc').limit(6).offset(0)
        response.json(shows)
    }

    * show(request, response) {
        const show = yield Show.find(request.param('id'))

        if (show.id === undefined) {
            response.notFound('Show not found')
        }
        else {
            const query = yield Database('watching').where('watching.show_id', show.id).avg('rating as rating').first()
            show.rating = Math.floor(query.rating)
            response.json(show.attributes)
        }
    }

    * store(request, response) {
        const data = request.all()
        const validation = yield Validator.validate(this.rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            const show = new Show()
            show.imdb_id = request.input('imdb_id')
            show.name = request.input('name')
            show.description = request.input('description')
            show.year = request.input('year')
            show.creator = request.input('creator')
            show.channel = request.input('channel')
            yield show.create()
            response.json(show.attributes)
        }
    }

    * update(request, response) {
        const data = request.all()
        const validation = yield Validator.validate(this.rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            const show = yield Show.find(request.param('id'))
            if (show.id === undefined) {
                response.notFound('Show not found')
            }
            else {
                show.imdb_id = request.input('imdb_id')
                show.name = request.input('name')
                show.description = request.input('description')
                show.year = request.input('year')
                show.creator = request.input('creator')
                show.channel = request.input('channel')
                yield show.update()
                response.json(show.attributes)
            }
        }
    }

    * destroy(request, response) {
        const show = yield Show.find(request.param('id'))
        if (show.id === undefined) {
            response.notFound('Show not found')
        }
        else {
            yield show.delete()
            response.ok()
        }
    }

    * watchers(request, response) {
        const watchers = yield Database('users').whereIn('id', function () {
            this.select('watching.user_id').from('watching').innerJoin('shows', 'watching.show_id', 'shows.id').where('shows.id', request.param('id'))
        })

        //const show = yield Show.where('id', request.param('id')).first().with('users').fetch();
        if (watchers === undefined) {
            response.notFound('Watchers not found')
        }
        else {
            response.json(watchers)
        }
    }

    * categories(request, response) {
        //const show = yield Show.where('id', request.param('id')).first().with('categories').fetch()
        const categories = yield Database('categories').whereIn('id', function () {
            this.select('category_show.category_id').from('category_show').innerJoin('shows', 'category_show.show_id', 'shows.id').where('shows.id', request.param('id'))
        })
        if (categories === undefined) {
            response.notFound('Show not found')
        }
        else {
            response.json(categories)
        }
    }

    * seasons(request, response) {
        //const show = yield Show.where('id', request.param('id')).first().with('seasons').fetch()
        const seasons = yield Database('seasons').where('show_id', request.param('id'))
        if (seasons === undefined) {
            response.notFound('Show not found')
        }
        else {
            response.json(seasons)
        }
    }

    * reviews(request, response) {
        const reviews = yield Database('reviews').where('show_id', request.param('id')).orderBy("created_at","desc")
        if (reviews === undefined) {
            response.notFound('Reviews not found')
            return
        }
        for (let i = 0; i < reviews.length; i++) {
            const user = yield Database('users').where('id', reviews[i].user_id).first()
            reviews[i].username = user.username;

            const messages = yield Database('review_comment').where('review_id', reviews[i].id).count('id as nr')
            reviews[i].number = messages[0].nr;
        }

        response.json(reviews)
    }

    * rate(request, response) {
        const validation = yield Validator.validate({
            rating: 'required|range:0,6'
        }, request.all())

        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            yield Database('watching').where({user_id: request.user_id, show_id: request.param('id')})
                .update({rating: request.input('rating')})
            response.ok()
        }
    }


    * upload(request, response) {
        const poster = request.file('poster')
        const file_name = request.param('id') + '.' + poster.extension()
        yield poster.move('./resources/posters', file_name)

        if (poster.moved()) {
            const show = yield Show.find(request.param('id'))
            show.poster = file_name
            yield show.update()
            response.ok('Poster uploaded')
        }
        else {
            response.status(405).send('Method not allowed')
        }
    }

    *poster(request, response) {
        const show = yield Database('shows').where('id', request.param('id')).first()
        if (show.poster === null) {
            response.download('./resources/posters/default.png')
            return
        }
        response.download('./resources/posters/' + show.poster)
    }

    *header(request, response) {
        const show = yield Database('shows').where('id', request.param('id')).first()
        response.download('./resources/headers/' + show.id + '.jpg')
    }

    *rating(request, response) {
        const rating = yield Database('watching').where('watching.show_id', request.param('id')).avg('rating as rating').first()
        response.json(rating)
    }

    *percent(request, response) {
        const show_id = request.param('id')

        const episodes = yield Database.select('id').from('episodes').whereIn('season_id', function () {
            this.select('id').from('seasons').where('show_id', show_id)
        }).count('id as count')

        const seen = yield Database('seen').where('user_id', request.user_id).whereIn('episode_id', function () {
            this.select('id').from('episodes').whereIn('season_id', function () {
                this.select('id').from('seasons').where('show_id', show_id)
            })
        }).count('user_id as count')

        const percent = Math.floor((seen[0].count * 100) / episodes[0].count)
        response.json({percent: percent})
    }


}

module.exports = ShowsController
