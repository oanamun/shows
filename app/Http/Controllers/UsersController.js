'use strict'

const User = use('App/Model/User')
const Show = use('App/Model/Show')
const Episode = use('App/Model/Episode')
const Notification = use('App/Model/Notification')
const Validator = use('Validator')
const Hash = use('Hash')
const Database = use('Database')

class UsersController {

    * index(request, response) {
        const users = yield User.all()
        response.json(users)
    }

    * show(request, response) {
        const id = request.param('id')
        const user = yield Database('users').where('id', id).first()

        if (user === undefined) {
            response.notFound('User not found')
            return;
        }

        const req_user = yield Database('users').where('id', request.user_id).first()
        if (user.role_id != 3 && req_user.role_id == 3) {
            response.unauthorized('You are not authorized to see this user!')
            return;
        }

        if (req_user.role_id == 3) {
            const rel = yield Database('friends').where('user_id', request.user_id).where('friend_id', id)
            if (rel.length) {
                user.following = true;
            }
            else {
                user.following = false;
            }
        }
        response.json(user)
    }

    * store(request, response) {
        const find = yield User.where('username', request.username).where('email', request.email).first().fetch()
        if (find.id) {
            response.badRequest('User already exists')
            return
        }
        const data = request.all()
        const rules = {
            username: 'required|alpha_numeric',
            password: 'required|min:6',
            email: 'required|email',
            first_name: 'alpha_numeric',
            last_name: 'alpha_numeric',
            role_id: 'alpha_numeric'
        }
        const validation = yield Validator.validate(rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
        }
        else {
            data.password = yield Hash.make(data.password)
            data.role_id = data.role_id || 3
            const user = new User(data)

            yield user.create()
            response.json(user.attributes)
        }
    }

    * update(request, response) {
        const data = request.all()
        const rules = {
            username: 'required|alpha_numeric',
            email: 'required|email',
            role_id: 'alpha_numeric'
        }
        const validation = yield Validator.validate(rules, data)
        if (validation.fails()) {
            response.badRequest(validation.messages())
            return;
        }
        const user = yield User.find(request.param('id'))
        if (user.id === undefined) {
            response.notFound('User not found')
            return;
        }

        const req_user = yield User.find(request.user_id)
        if (request.user_id != user.id && req_user.role_id == 3) {
            response.unauthorized('You cannot edit this user!')
            return;
        }
        user.username = request.input('username')
        user.email = request.input('email')
        user.first_name = request.input('first_name')
        user.last_name = request.input('last_name')
        user.role_id = request.input('role_id') || user.role_id
        yield user.update()
        response.json(user.attributes)

    }

    * destroy(request, response) {
        const user = yield User.find(request.param('id'))
        if (user.id === undefined) {
            response.notFound('User not found')
        } else {
            yield user.delete()
            response.ok()
        }
    }

    * watching(request, response) {
        var id = request.param('id') || request.user_id
        const watching = yield Database('shows').whereIn('id', function () {
            this.select('watching.show_id').from('watching').innerJoin('users', 'watching.user_id', 'users.id').where('users.id', id)
        })
        if (watching === undefined) {
            response.notFound('Watchlist not found')
        }
        else {
            response.json(watching)
        }
    }

    *isWatching(request, response) {
        let rel;
        if (request.input('show_id')) {
            rel = yield Database('watching').where('user_id', request.user_id).where('show_id', request.input('show_id'))
        } else if (request.input('episode_id')) {
            rel = yield Database('seen').where('user_id', request.user_id).where('episode_id', request.input('episode_id'))
        }

        if (rel.length) {
            response.json({watching: true})
        } else {
            response.json({watching: false})
        }
    }

    * watch(request, response) {
        const user = yield User.find(request.user_id)
        const show_id = request.input('show_id')
        const rel = yield Database('watching').where('user_id', request.user_id).where('show_id', show_id)

        if (rel.length) {
            yield user.shows().detach(show_id)
        }
        else {
            yield user.shows().attach(show_id)
        }
        response.ok()
    }


    * seen(request, response) {
        var id = request.param('id') || request.user_id
        const watching = yield Database('episodes').whereIn('id', function () {
            this.select('seen.episode_id').from('seen').innerJoin('users', 'seen.user_id', 'users.id').where('users.id', id)
        })
        if (watching === undefined) {
            response.notFound('Episodes not found')
        }
        else {
            response.json(watching)
        }
    }

    * see(request, response) {
        const user = yield User.find(request.user_id)
        const episode_id = request.input('episode_id')

        const rel = yield Database('seen').where('user_id', request.user_id).where('episode_id', episode_id)

        if (rel.length) {
            yield user.episodes().detach(episode_id)
        }
        else {
            yield user.episodes().attach(episode_id)
        }
        response.ok()
    }

    * unsee(request, response) {
        const user = yield User.find(request.user_id)
        const episode_id = request.input('episode_id')
        yield user.episodes().detach(episode_id)
        response.ok()
    }

    * following(request, response) {
        var id = request.param('id')
        const friends = yield Database('users').whereIn('id', function () {
            this.select('friends.friend_id').from('users').innerJoin('friends', 'users.id', 'friends.user_id').where('users.id', id)
        })
        if (friends === undefined) {
            response.notFound('Friends not found')
        }
        else {
            response.json(friends)
        }
    }

    * followers(request, response) {
        var id = request.param('id')
        const friends = yield Database('users').whereIn('id', function () {
            this.select('friends.user_id').from('users').innerJoin('friends', 'users.id', 'friends.user_id').where('friends.friend_id', id)
        })
        if (friends === undefined) {
            response.notFound('Friends not found')
        }
        else {
            response.json(friends)
        }
    }

    * follow(request, response) {
        const user = yield User.find(request.user_id)
        const friend_id = request.input('friend_id')

        const rel = yield Database('friends').where('user_id', request.user_id).where('friend_id', friend_id)

        if (rel.length) {
            yield user.friends().detach(friend_id)
        }
        else {
            yield user.friends().attach(friend_id)
            const notification = new Notification()
            notification.user_id = friend_id
            notification.type = 'follow'
            notification.follower_id = user.id
            yield notification.create()
        }

        response.ok()

    }

    * reviews(request, response) {
        var id = request.param('id')
        const reviews = yield Database('reviews').where('user_id', id)
        if (reviews === undefined) {
            response.notFound('Reviews not found')
        }
        else {
            response.json(reviews)
        }
    }

    * notifications(request, response) {
        var id = request.user_id
        const notifications = yield Database('notifications').where('user_id', id)
        if (notifications === undefined) {
            response.notFound('Notifications not found')
            return
        }

        for (let i = 0; i < notifications.length; i++) {
            if (notifications[i].follower_id) {
                const user = yield Database('users').where('id', notifications[i].follower_id).first()
                notifications[i].follower = user.username
            }
            if (notifications[i].show_id) {
                const show = yield Database('shows').where('id', notifications[i].show_id).first()
                notifications[i].show = show.name
            }
            if (notifications[i].episode_id) {
                const episode = yield Database('episodes').where('id', notifications[i].episode_id).first()
                notifications[i].episode = episode.name
            }
        }

        response.json(notifications)
    }

    * activity(request, response) {
        var id = request.user_id

        const reviews = yield Database('reviews').whereIn('user_id', function () {
            this.select('users.id').from('users').whereIn('id', function () {
                this.select('friends.friend_id').from('users').innerJoin('friends', 'users.id', 'friends.user_id').where('users.id', id)
            })
        }).orderBy('created_at', 'desc')

        const discussions = yield Database('discussions').whereIn('user_id', function () {
            this.select('users.id').from('users').whereIn('id', function () {
                this.select('friends.friend_id').from('users').innerJoin('friends', 'users.id', 'friends.user_id').where('users.id', id)
            })
        }).orderBy('created_at', 'desc')

        const watched = yield Database('watching').whereIn('user_id', function () {
            this.select('users.id').from('users').whereIn('id', function () {
                this.select('friends.friend_id').from('users').innerJoin('friends', 'users.id', 'friends.user_id').where('users.id', id)
            })
        }).orderBy('created_at', 'desc')

        const saw = yield Database('seen').whereIn('user_id', function () {
            this.select('users.id').from('users').whereIn('id', function () {
                this.select('friends.friend_id').from('users').innerJoin('friends', 'users.id', 'friends.user_id').where('users.id', id)
            })
        }).orderBy('created_at', 'desc')

        let activity = []
        for (let i = 0; i < reviews.length; i++) {
            const user = yield User.find(reviews[i].user_id)
            const show = yield Show.find(reviews[i].show_id)
            activity.push({
                type: 'review',
                review_id: reviews[i].id,
                user_id: reviews[i].user_id,
                user: user.username,
                show_id: reviews[i].show_id,
                show: show.name,
                title: reviews[i].title,
                date: reviews[i].created_at
            })
        }

        for (let i = 0; i < discussions.length; i++) {
            const user = yield User.find(discussions[i].user_id)
            const episode = yield Episode.find(discussions[i].episode_id)
            activity.push({
                type: 'discussion',
                discussion_id: discussions[i].id,
                user_id: discussions[i].user_id,
                user: user.username,
                episode_id: discussions[i].episode_id,
                episode: episode.name,
                title: discussions[i].title,
                date: discussions[i].created_at
            })
        }
        for (let i = 0; i < watched.length; i++) {
            const user = yield User.find(watched[i].user_id)
            const show = yield Show.find(watched[i].show_id)
            activity.push({
                type: 'watched',
                user_id: watched[i].user_id,
                user: user.username,
                show_id: watched[i].show_id,
                show: show.name,
                date: watched[i].created_at
            })
        }
        for (let i = 0; i < saw.length; i++) {
            const user = yield User.find(saw[i].user_id)
            const episode = yield Episode.find(saw[i].episode_id)
            activity.push({
                type: 'saw',
                user_id: saw[i].user_id,
                user: user.username,
                episode_id: saw[i].episode_id,
                episode: episode.name,
                date: saw[i].created_at
            })
        }

        activity.sort((a, b) => {
            if (a.date < b.date)
                return 1;
            else if (a.date > b.date)
                return -1;
            else
                return 0;
        })


        activity.splice(20, activity.length - 20)

        response.json(activity)

    }

    * reset(request, response) {
        const user = yield User.find(request.user_id)
        const isSame = yield Hash.verify(request.input('old_password'), user.password)
        if (isSame) {
            user.password = request.input('new_password')
            yield user.update()
            response.ok()
        }
        else {
            response.badRequest('Old password is incorrect!')
        }

    }

    * upload(request, response) {
        const picture = request.file('picture')
        const file_name = request.user_id + '.' + picture.extension()
        yield picture.move('./resources/pictures', file_name)

        if (picture.moved()) {
            const user = yield User.find(request.user_id)
            user.picture = file_name
            yield user.update()
            response.ok('Picture uploaded')
        }
        else {
            response.status(405).send('Method not allowed')
        }
    }

    *picture(request, response) {
        var id = request.param('id')
        const user = yield Database('users').where('id', id).first()
        if (user.picture === null) {
            response.download('./resources/pictures/default.jpg')
            return
        }
        response.download('./resources/pictures/' + user.picture)
    }

    *myRating(request, response) {
        if (request.input('show_id')) {
            const rel = yield Database('watching').where('user_id', request.user_id).where('show_id', request.input('show_id'))
            if (rel[0]) {
                response.json({rating: rel[0].rating})
                return;
            }
            response.notFound('Show not found in watchlist!')
        }
        else if (request.input('episode_id')) {
            const rel = yield Database('seen').where('user_id', request.user_id).where('episode_id', request.input('episode_id'))
            if (rel[0]) {
                response.json({rating: rel[0].rating})
                return;
            }
            response.notFound('Episode not found in watchlist!')
        }
    }

    *about(request, response) {
        const id = request.param('id');
        const watching = yield Database('watching').where('user_id', id).count('user_id as nr')
        const seen = yield Database('seen').where('user_id', id).count('user_id as nr')
        const reviews = yield Database('reviews').where('user_id', id).count('user_id as nr')
        const discussions = yield Database('discussions').where('user_id', id).count('user_id as nr')
        const followers = yield Database('friends').where('friend_id', id).count('user_id as nr')
        const following = yield Database('friends').where('user_id', id).count('user_id as nr')

        let about = {
            watching: watching[0].nr,
            seen: seen[0].nr,
            reviews: reviews[0].nr,
            discussions: discussions[0].nr,
            followers: followers[0].nr,
            following: following[0].nr
        }
        response.json(about);
    }

    *recommend(request, response) {
        const user = yield User.find(request.user_id)
        const notification = new Notification()
        notification.user_id = request.input('friend_id')
        notification.type = 'recommendation'
        notification.follower_id = user.id
        notification.show_id = request.input('show_id')
        yield notification.create()

        response.ok()
    }

}

module.exports = UsersController
