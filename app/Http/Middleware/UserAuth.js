'use strict'
const JWT = use('jwt-simple')
const Env = use('Env')
const User = use('App/Model/User')
const Config = use('Config')

class UserAuth {

    *handle(request, response, next) {
        const token = request.header('Token', '')
        if (token == undefined) {
            response.badRequest()
            return
        }
        const jwt = JWT.decode(token, Env.get('JWT_SECRET'));
        if (jwt.created_at < Math.floor(Date.now() / 1000) - Config.get('jwt.expire_after')) {
            response.unauthorized('Token expired')
            return;
        }
        const user = yield User.find(jwt.id)
        if (user.role_id != 3) {
            response.unauthorized()
        }
        else {
            request.user_id = jwt.id
            yield next
        }
    }

}

module.exports = UserAuth
