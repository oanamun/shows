'use strict'

const Middleware = use('Middleware')

const globalMiddleware = [
  'Adonis/Middleware/Cors',
  'Adonis/Middleware/BodyParser',
  //'Adonis/Middleware/Shield',
  'Adonis/Middleware/Flash'
]

const namedMiddleware = {
  'admin_auth' : 'App/Http/Middleware/AdminAuth',
  'user_auth' : 'App/Http/Middleware/UserAuth',
  'auth' : 'App/Http/Middleware/Auth'
}

Middleware.global(globalMiddleware)
Middleware.named(namedMiddleware)
