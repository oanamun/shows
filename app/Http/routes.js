'use strict'

const Route = use('Route')

Route.get('/', 'HomeController.index')
Route.post('api/login', 'LoginController.login')

//ROLES--------------------------------------------------------------

Route.group('roles', function () {
    Route.get('api/roles', 'RolesController.index')
    Route.get('api/roles/:id', 'RolesController.show')
    Route.get('api/roles/:id/users', 'RolesController.users')
}).middlewares(['admin_auth']);

//USERS--------------------------------------------------------------

Route.post('api/users', 'UsersController.store')
Route.get('api/users/:id/picture', 'UsersController.picture')

Route.group('user_admin', function () {
    Route.get('api/users', 'UsersController.index')
    Route.delete('api/users/:id', 'UsersController.destroy')
}).middlewares(['admin_auth']);

Route.group('user_auth', function () {
    Route.put('api/users/:id', 'UsersController.update')
    Route.get('api/user/:id', 'UsersController.show')
    Route.get('api/users/:id?/watching', 'UsersController.watching')
    Route.get('api/users/:id/seen', 'UsersController.seen')
    Route.get('api/users/:id/followers', 'UsersController.followers')
    Route.get('api/users/:id/following', 'UsersController.following')
    Route.get('api/users/:id/reviews', 'UsersController.reviews')
    Route.get('api/users/:id/about', 'UsersController.about')
}).middlewares(['user_auth']);

Route.group('user_actions', function () {
    Route.get('api/users/isWatching', 'UsersController.isWatching')
    Route.get('api/users/notifications', 'UsersController.notifications')
    Route.get('api/users/activity', 'UsersController.activity')
    Route.get('api/users/myRating', 'UsersController.myRating')
    Route.post('api/users/upload', 'UsersController.upload')
    Route.post('api/users/watch', 'UsersController.watch')
    Route.post('api/users/see', 'UsersController.see')
    Route.post('api/users/follow', 'UsersController.follow')
    Route.post('api/users/reset', 'UsersController.reset')
    Route.post('api/shows/recommend', 'UsersController.recommend')
}).middlewares(['user_auth']);

//SHOWS--------------------------------------------------------------

Route.get('api/shows', 'ShowsController.index')
Route.get('api/shows/recent', 'ShowsController.recent')
Route.get('api/shows/:id', 'ShowsController.show')

Route.group('shows_admin', function () {
    Route.post('api/shows', 'ShowsController.store')
    Route.post('api/shows/:id/upload', 'ShowsController.upload')
    Route.put('api/shows/:id', 'ShowsController.update')
    Route.delete('api/shows/:id', 'ShowsController.destroy')
}).middlewares(['admin_auth']);

Route.get('api/shows/:id/poster', 'ShowsController.poster')
Route.get('api/shows/:id/header', 'ShowsController.header')
Route.get('api/shows/:id/watchers', 'ShowsController.watchers').middlewares(['user_auth'])
Route.get('api/shows/:id/categories', 'ShowsController.categories')
Route.get('api/shows/:id/seasons', 'ShowsController.seasons')
Route.get('api/shows/:id/reviews', 'ShowsController.reviews')
Route.get('api/shows/:id/rating', 'ShowsController.rating')
Route.get('api/shows/:id/percent', 'ShowsController.percent').middlewares(['user_auth'])
Route.post('api/shows/:id/rate', 'ShowsController.rate').middlewares(['user_auth'])

//SEASONS--------------------------------------------------------------

Route.get('api/seasons', 'SeasonsController.index')
Route.get('api/seasons/:id', 'SeasonsController.show')
Route.post('api/seasons', 'SeasonsController.store').middlewares(['admin_auth'])
Route.put('api/seasons/:id', 'SeasonsController.update').middlewares(['admin_auth'])
Route.delete('api/seasons/:id', 'SeasonsController.destroy').middlewares(['admin_auth'])
Route.get('api/seasons/:id/episodes', 'SeasonsController.episodes')

//EPISODES--------------------------------------------------------------

Route.get('api/episodes', 'EpisodesController.index')
Route.get('api/episodes/:id', 'EpisodesController.show')
Route.post('api/episodes', 'EpisodesController.store').middlewares(['admin_auth'])
Route.put('api/episodes/:id', 'EpisodesController.update').middlewares(['admin_auth'])
Route.delete('api/episodes/:id', 'EpisodesController.destroy').middlewares(['admin_auth'])
Route.get('api/episodes/:id/watchers', 'EpisodesController.watchers')
Route.get('api/episodes/:id/discussions', 'EpisodesController.discussions')
Route.post('api/episodes/:id/rate', 'EpisodesController.rate').middlewares(['user_auth'])
Route.get('api/episodes/:id/rating', 'EpisodesController.rating')

//CATEGORIES--------------------------------------------------------------

Route.get('api/categories', 'CategoriesController.index')
Route.get('api/categories/:id', 'CategoriesController.show')
Route.post('api/categories', 'CategoriesController.store').middlewares(['admin_auth'])
Route.put('api/categories/:id', 'CategoriesController.update').middlewares(['admin_auth'])
Route.delete('api/categories/:id', 'CategoriesController.destroy').middlewares(['admin_auth'])
Route.get('api/categories/:id/shows', 'CategoriesController.shows')
Route.post('api/categories/show', 'CategoriesController.addshow')

//REVIEWS--------------------------------------------------------------

Route.get('api/reviews', 'ReviewsController.index')
Route.get('api/reviews/recent', 'ReviewsController.recent')
Route.get('api/reviews/:id', 'ReviewsController.show')
Route.post('api/reviews', 'ReviewsController.store').middlewares(['user_auth'])
Route.put('api/reviews/:id', 'ReviewsController.update').middlewares(['user_auth'])
Route.delete('api/reviews/:id', 'ReviewsController.destroy').middlewares(['admin_auth'])
Route.get('api/reviews/:id/comments', 'ReviewsController.comments')
Route.get('api/reviews/:id/ratings', 'ReviewsController.ratings')
Route.post('api/reviews/:id/rate', 'ReviewsController.rate').middlewares(['user_auth'])

//REVIEW COMMENTS--------------------------------------------------------------

Route.get('api/rcomments', 'ReviewCommentsController.index')
Route.get('api/rcomments/:id', 'ReviewCommentsController.show')
Route.post('api/rcomments', 'ReviewCommentsController.store').middlewares(['user_auth'])
Route.put('api/rcomments/:id', 'ReviewCommentsController.update').middlewares(['user_auth'])
Route.delete('api/rcomments/:id', 'ReviewCommentsController.destroy').middlewares(['admin_auth'])

//DISCUSSIONS--------------------------------------------------------------

Route.get('api/discussions', 'DiscussionsController.index')
Route.get('api/discussions/:id', 'DiscussionsController.show')
Route.post('api/discussions', 'DiscussionsController.store').middlewares(['user_auth'])
Route.put('api/discussions/:id', 'DiscussionsController.update').middlewares(['user_auth'])
Route.delete('api/discussions/:id', 'DiscussionsController.destroy').middlewares(['admin_auth'])
Route.get('api/discussions/:id/comments', 'DiscussionsController.comments')

//DISCUSSION COMMENTS--------------------------------------------------------------

Route.get('api/dcomments', 'DiscussionCommentsController.index')
Route.get('api/dcomments/:id', 'DiscussionCommentsController.show')
Route.post('api/dcomments', 'DiscussionCommentsController.store').middlewares(['user_auth'])
Route.put('api/dcomments/:id', 'DiscussionCommentsController.update').middlewares(['user_auth'])
Route.delete('api/dcomments/:id', 'DiscussionCommentsController.destroy').middlewares(['admin_auth'])

//SEEDS--------------------------------------------------------------

Route.group('seeds', function () {
    Route.get('seed/users', 'SeedController.users')
    Route.get('seed/shows', 'SeedController.shows')
    Route.get('seed/seasons', 'SeedController.seasons')
    Route.get('seed/episodes', 'SeedController.episodes')
    Route.get('seed/reviews', 'SeedController.reviews')
    Route.get('seed/categories', 'SeedController.categories')
    Route.get('seed/discussions', 'SeedController.discussions')
    Route.get('seed/roles', 'SeedController.roles')
});
