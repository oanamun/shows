'use strict'

const Lucid = use("Lucid")

class Category extends Lucid {
    static get table () {
        return 'categories'
    }
    static get softDeletes() {
        return false
    }
    static get timestamps() {
        return false;
    }
    shows () {
        return this.belongsToMany('App/Model/Show')
    }
}

module.exports = Category
