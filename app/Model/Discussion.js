'use strict'

const Lucid = use("Lucid")

class Discussion extends Lucid {
    static get softDeletes() {
        return false
    }

    comments(){
        return this.hasMany('App/Model/DiscussionComment')
    }
}

module.exports = Discussion
