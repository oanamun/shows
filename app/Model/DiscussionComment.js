'use strict'

const Lucid = use("Lucid")

class DiscussionComment extends Lucid {
    static get table () {
        return 'discussion_comment'
    }
    static get softDeletes() {
        return false
    }
}

module.exports = DiscussionComment
