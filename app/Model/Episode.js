'use strict'

const Lucid = use("Lucid")

class Episode extends Lucid {
    static get softDeletes() {
        return false
    }
    users() {
        return this.belongsToMany('App/Model/User', 'seen').withPivot('rating')
    }

    discussions() {
        return this.hasMany('App/Model/Discussion')
    }
}

module.exports = Episode
