'use strict'

const Lucid = use("Lucid")

class Notification extends Lucid {
    static get softDeletes() {
        return false
    }

}

module.exports = Notification
