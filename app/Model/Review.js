'use strict'

const Lucid = use("Lucid")

class Review extends Lucid {
    static get softDeletes() {
        return false
    }

    comments(){
        return this.hasMany('App/Model/ReviewComment')
    }

    ratings(){
        return this.belongsToMany('App/Model/User','user_review').withPivot('rating')
    }
}

module.exports = Review
