'use strict'

const Lucid = use("Lucid")

class ReviewComment extends Lucid {
    static get table () {
        return 'review_comment'
    }
    static get softDeletes() {
        return false
    }
}

module.exports = ReviewComment
