'use strict'

const Lucid = use("Lucid")

class Role extends Lucid {
    static get softDeletes() {
        return false
    }
    static get timestamps() {
        return false;
    }
    users () {
        return this.hasMany('App/Model/User')
    }
}

module.exports = Role
