'use strict'

const Lucid = use("Lucid")

class Season extends Lucid {
    static get softDeletes() {
        return false
    }

    episodes () {
        return this.hasMany('App/Model/Episode')
    }
}

module.exports = Season
