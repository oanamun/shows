'use strict'

const Lucid = use("Lucid")

class Show extends Lucid {
    static get softDeletes() {
        return false
    }

    categories () {
        return this.belongsToMany('App/Model/Category')
    }

    users () {
        return this.belongsToMany('App/Model/User','watching').withPivot('rating')
    }

    seasons () {
        return this.hasMany('App/Model/Season')
    }

    reviews(){
        return this.hasMany('App/Model/Review')
    }
}

module.exports = Show