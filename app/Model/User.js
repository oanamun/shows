'use strict'

const Lucid = use("Lucid")

class User extends Lucid {
    static get softDeletes() {
        return false
    }

    static get hidden(){
        return ['password'];
    }

    shows() {
        return this.belongsToMany('App/Model/Show', 'watching').withPivot('rating','created_at')
    }

    episodes() {
        return this.belongsToMany('App/Model/Episode', 'seen').withPivot('rating','created_at')
    }

    friends() {
        return this.belongsToMany('App/Model/User', 'friends', 'user_id', 'friend_id')
    }

    reviews(){
        return this.hasMany('App/Model/Review')
    }

    notifications(){
        return this.hasMany('App/Model/Notification')
    }

    discussions(){
        return this.hasMany('App/Model/Discussion')
    }

}

module.exports = User
