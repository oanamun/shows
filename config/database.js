'use strict'

const Helpers = use('Helpers')
const Env = use('Env')

module.exports = {

  connection: Env.get('DB_CONNECTION'),

   migrationsTable: 'adonis_schema',

  sqlite: {
    client: 'sqlite3',
    connection: {
      filename: Helpers.storagePath('development.sqlite3')
    },
    debug: false
  },

  mysql: {
    client: 'mysql',
    connection: {
      host: Env.get('MYSQL_HOST'),
      user: Env.get('MYSQL_USER'),
      password: Env.get('MYSQL_PASSWORD'),
      database: Env.get('MYSQL_DATABASE')
    }
  }

}
