'use strict'

module.exports = {
    /**
     * After how many second the token will expire.
     */
    expire_after: 432000, //5 days
}
